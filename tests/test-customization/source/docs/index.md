# Welcome to MkDocs

For full documentation visit [mkdocs.org](https://www.mkdocs.org).

## Commands

* `mkdocs new [dir-name]` - Create a new project.
* `mkdocs serve` - Start the live-reloading docs server.
* `mkdocs build` - Build the documentation site.
* `mkdocs -h` - Print help message and exit.

## Project layout

    mkdocs.yml    # The configuration file.
    docs/
        index.md  # The documentation homepage.
        ...       # Other markdown pages, images and other files.

## Extensions

### plantuml_markdown

```plantuml format="svg_inline" alt="My sequence diagram placeholder" title="My sequence diagram"
Goofy ->  MickeyMouse: calls
Goofy <-- MickeyMouse: responds
```

```plantuml format="svg_inline" alt="My class diagram placeholder" title="My class diagram"
    Class01 <|-- Class02
    Class03 *-- Class04
    Class05 o-- Class06
    Class07 .. Class08
    Class09 -- Class10
```

## Plugins

### mkdocs-awesome-pages-plugin

See the site structure.

### mkdocs-build-plantuml-plugin

![UML diagram](diagrams/images/test.png)

### mkdocs-enumerate-headings-plugin

See the document titles.

### mkdocs-git-revision-date-localized-plugin

No need to test this plugin for now.

### mkdocs-git-revision-date-plugin

No need to test this plugin for now.

### mkdocs-include-markdown-plugin

{%
   include-markdown "included.md"
   start="<!--inclusion-start-->"
   end="<!--inclusion-end-->"
%}

### mkdocs-macros-plugin

The unit price is {{ unit.price }}

### mkdocs-mermaid2-plugin

```mermaid
graph LR
  A[Start] --> B{Error?};
  B -->|Yes| C[Hmm...];
  C --> D[Debug];
  D --> B;
  B ---->|No| E[Yay!];
```
