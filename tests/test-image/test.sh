#!/usr/bin/env bash

PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../.." && pwd )"

# shellcheck disable=SC1090
source "${PROJECT_DIR}/src/files/usr/share/ddidier/shflags/shflags"
# shellcheck disable=SC1090
source "${PROJECT_DIR}/src/files/usr/share/ddidier/ansi/ansi"
# shellcheck disable=SC1090
source "${PROJECT_DIR}/src/files/usr/share/ddidier/ndd-log4b/ndd-log4b.sh"
# shellcheck disable=SC1090
source "${PROJECT_DIR}/src/files/usr/share/ddidier/ndd-utils4b/ndd-utils4b.sh"

# disable before shflags
ndd::base::catch_more_errors_off

# shellcheck disable=SC2034
DEFINE_boolean  "meld"   false  "Open Meld on comparison failures"  "m"
DEFINE_boolean  "debug"  false  "Enable debug mode"                 "d"

# parse the command-line
FLAGS "$@" || exit $?
eval set -- "${FLAGS_ARGV}"

# enable after shflags
ndd::base::catch_more_errors_on



function main() {

    if [[ "${FLAGS_debug}" -eq "${FLAGS_TRUE}" ]]; then
        ndd::logger::set_stdout_level "DEBUG"
    else
        ndd::logger::set_stdout_level "INFO"
    fi

    log info "Testing the Docker image 'ddidier/mkdocs:latest'"

    # $UID and $USER are not defined in Alpine image
    # local container_uid="${UID}"
    # local container_username="${USER}"
    local container_uid
    container_uid="$(id -u)"

    # Retrieve Git metadata from the Docker image
    local docker_image_sha
    local docker_image_tag
    docker_image_sha="$(docker run --rm -e USER_ID="${container_uid}" "ddidier/mkdocs:latest" bash -c 'echo "${GIT_COMMIT_SHA}"')"
    docker_image_tag="$(docker run --rm -e USER_ID="${container_uid}" "ddidier/mkdocs:latest" bash -c 'echo "${GIT_COMMIT_TAG}"')"
    log debug "Docker image Git commit = ${docker_image_sha}"
    log debug "Docker image Git tag    = ${docker_image_tag}"

    # Retrieve Git metadata from the local repository
    local git_commit_sha
    local git_commit_tag
    git_commit_sha="$(git log -1 --pretty=%H)"
    git_commit_tag="$(git describe --exact-match --tags "${git_commit_sha}" 2> /dev/null || echo "testing")"
    log debug "Git repository commit   = ${git_commit_sha}"
    log debug "Git repository tag      = ${git_commit_tag}"

    # Compare Git metadata between the Docker image and the local repository
    if [[ "${docker_image_sha}" != "${git_commit_sha}" ]]; then
        log error "Wrong or missing Git commit hash in the Docker image:"
        log error "- in Git repository = ${git_commit_sha}"
        log error "- in Docker image   = ${docker_image_sha}"
        log error "Have you rebuild your image after committing?"
        log error "Docker image test: FAILED"
        exit 1
    fi

    if [[ "${docker_image_tag}" != "${git_commit_tag}" ]]; then
        log error "Wrong or missing Git tag in the Docker image:"
        log error "- in Git repository = ${git_commit_tag}"
        log error "- in Docker image   = ${docker_image_tag}"
        log error "Docker image test: FAILED"
        exit 1
    fi

    log info "┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"
    log info "┃ Docker image 'ddidier/mkdocs:latest' test: SUCCESS"
    log info "┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"
}

function error_handler() {
    local error_code="$?"

    test $error_code == 0 && return;

    log error "An unexpected error has occured:\n%s" "$(ndd::base::print_stack_trace 2>&1)"

    exit 1
}

trap 'error_handler ${?}' ERR

main "${@}"
