#!/usr/bin/env bash

PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." && pwd )"

# shellcheck disable=SC1090
source "${PROJECT_DIR}/src/files/usr/share/ddidier/ansi/ansi"
# shellcheck disable=SC1090
source "${PROJECT_DIR}/src/files/usr/share/ddidier/ndd-log4b/ndd-log4b.sh"
# shellcheck disable=SC1090
source "${PROJECT_DIR}/src/files/usr/share/ddidier/ndd-utils4b/ndd-utils4b.sh"

ndd::base::catch_more_errors_on

ndd::logger::set_stdout_level "INFO"



if [[ ! -f "${PROJECT_DIR}/lib/shellcheck/shellcheck" ]]; then

  log error "┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"
  log error "┃ Quality check requires ShellCheck (https://github.com/koalaman/shellcheck)   "
  log error "┃ to be installed! Use 'make setup' to install required dependencies.          "
  log error "┃ Aborting...                                                                  "
  log error "┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"

  exit 1

fi



if find "${PROJECT_DIR}" -type f -name "*.sh" -print0 | xargs -0 "${PROJECT_DIR}/lib/shellcheck/shellcheck" \
&& grep -r -l '^#!/usr/bin/env bash' "${PROJECT_DIR}" | xargs "${PROJECT_DIR}/lib/shellcheck/shellcheck"; then

  log info  "┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"
  log info  "┃ QUALITY -- Perfect!                                                          "
  log info  "┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"

else

  log error "┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"
  log error "┃ QUALITY -- There is still some work to do...                                 "
  log error "┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"
  exit 1

fi
