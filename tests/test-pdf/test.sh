#!/usr/bin/env bash

PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../.." && pwd )"

# shellcheck disable=SC1090
source "${PROJECT_DIR}/src/files/usr/share/ddidier/shflags/shflags"
# shellcheck disable=SC1090
source "${PROJECT_DIR}/src/files/usr/share/ddidier/ansi/ansi"
# shellcheck disable=SC1090
source "${PROJECT_DIR}/src/files/usr/share/ddidier/ndd-log4b/ndd-log4b.sh"
# shellcheck disable=SC1090
source "${PROJECT_DIR}/src/files/usr/share/ddidier/ndd-utils4b/ndd-utils4b.sh"

# disable before shflags
ndd::base::catch_more_errors_off

# shellcheck disable=SC2034
DEFINE_boolean  "meld"   false  "Open Meld on comparison failures"  "m"
DEFINE_boolean  "debug"  false  "Enable debug mode"                 "d"

# parse the command-line
FLAGS "$@" || exit $?
eval set -- "${FLAGS_ARGV}"

# enable after shflags
ndd::base::catch_more_errors_on



function main() {

    if [[ "${FLAGS_debug}" -eq "${FLAGS_TRUE}" ]]; then
        ndd::logger::set_stdout_level "DEBUG"
    else
        ndd::logger::set_stdout_level "INFO"
    fi

    log info "Testing the PDF generation"

    # ---------- create temporary directories

    local temp_dir
    local temp_dir_link
    temp_dir=$(mktemp -d --suffix=.docker-mkdocs)
    temp_dir_link="$(dirname "${temp_dir}")/docker-mkdocs"

    rm -rf "${temp_dir_link}"
    ln -sf "${temp_dir}" "${temp_dir_link}"

    log info "Test data will be stored in the temporary directory: ${temp_dir}"
    log info "Use the following link for convenience: ${temp_dir_link}"

    # ----------

    log info "Copying default project files"
    cp -r "${PROJECT_DIR}"/tests/test-new-project/expected/* "${temp_dir}/"

    log info "Copying customized project files"
    cp -r "${PROJECT_DIR}"/tests/test-customization/source/* "${temp_dir}/"

    log info "Copying custom project files"
    cp -r "${PROJECT_DIR}"/tests/test-pdf/source/* "${temp_dir}/"

    # ----------

    # Tag the Docker image for testing
    log info "Tagging image 'ddidier/mkdocs:latest' with 'ddidier/mkdocs:testing'"
    docker tag "ddidier/mkdocs:latest" "ddidier/mkdocs:testing"

    log info "Generating HTML documentation"
    log debug "$(ndd::print::script_output_start)"
    while read -r line; do
        log debug "${line}"
    done < <( "${temp_dir}/bin/build-pdf.sh" )
    log debug "$(ndd::print::script_output_end)"

    # ----------

    log info "Checking PDF generation with mkdocs-with-pdf"
    actual_pdf_file_path="${temp_dir}/site/pdf/my-documentation-with-pdf.pdf"

    if [[ ! -f "${actual_pdf_file_path}" ]]; then
        log error "Missing PDF file: ${actual_pdf_file_path}"
        exit 1
    fi

    # hash comparison won't work (dates, etc.)
    local actual_pdf_file_size
    local expected_pdf_file_size=46864
    local expected_pdf_file_size_min=$(( expected_pdf_file_size * 99/100 ))
    local expected_pdf_file_size_max=$(( expected_pdf_file_size * 101/100 ))

    actual_pdf_file_size=$(stat --format="%s" "${actual_pdf_file_path}")

    log debug "actual_pdf_file_size       = ${actual_pdf_file_size}"
    log debug "expected_pdf_file_size_min = ${expected_pdf_file_size_min}"
    log debug "expected_pdf_file_size_max = ${expected_pdf_file_size_max}"
    # cp "${actual_pdf_file_path}" /tmp/

    if [[ ${actual_pdf_file_size} -lt ${expected_pdf_file_size_min} ]] || [[ ${actual_pdf_file_size} -gt ${expected_pdf_file_size_max} ]]; then
        log error "Wrong size of the PDF file: ${actual_pdf_file_path}"
        log error "Expected ${expected_pdf_file_size} but was ${actual_pdf_file_size}"
        exit 1
    fi

    # ----------

    log info "┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"
    log info "┃ PDF generation test: SUCCESS"
    log info "┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"
}

function error_handler() {
    local error_code="$?"

    test $error_code == 0 && return;

    log error "An unexpected error has occured:\n%s" "$(ndd::base::print_stack_trace 2>&1)"

    exit 1
}

trap 'error_handler ${?}' ERR

main "${@}"
